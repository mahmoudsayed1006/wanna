import express from 'express';
import adminController from '../../controllers/admin/admin.controller';

const router = express.Router();
router.route('/users')
    .get(adminController.getLastUser);

router.route('/orders')
    .get(adminController.getLastOrder);

router.route('/actions')
    .get(adminController.getLastActions);

router.route('/count')
    .get(adminController.count);





export default router;
