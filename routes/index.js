import express from 'express';
import userRoute from './user/user.route';
import CategoryRoute  from './category/category.route';
import CouponRoute  from './coupon/coupon.route';
import TaxRoute  from './tax/tax.route';
import DebtRoute  from './debt/debt.route';

import TimeRoute  from './time/time.route';
import YearRoute  from './year/year.route';
import TypeRoute  from './type/type.route';
import BankRoute  from './bank/bank.route';
import ShopRoute  from './shop/shop.route';
import OrderRoute  from './order/order.route';
import OfferRoute  from './offer/offer.route';
import BillRoute  from './bills/bill.route';
import messageRoute  from './message/message.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AdminRoute  from './admin/admin.route';
import AboutRoute  from './about/about.route';
import ProblemRoute  from './problem/problem.route';
import OptionRoute  from './option/option.route';

import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);

router.use('/orders',OrderRoute);
router.use('/offers',OfferRoute);
router.use('/bills',BillRoute);
router.use('/categories',CategoryRoute);
router.use('/shops',ShopRoute);
router.use('/coupons',CouponRoute);
router.use('/tax',TaxRoute);
router.use('/debt',DebtRoute);
router.use('/option',OptionRoute);
router.use('/time',TimeRoute);
router.use('/bank',BankRoute);
router.use('/type',TypeRoute);
router.use('/year',YearRoute);
router.use('/problem',ProblemRoute);

router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/about',AboutRoute);
router.use('/messages',messageRoute);

export default router;
