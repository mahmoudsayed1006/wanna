import express from 'express';
import OrderController from '../../controllers/order/order.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('orders').single('img'),
        OrderController.validateCreated(),
        OrderController.create
    );

router.route('/')
    .get(OrderController.findOrders)

router.route('/:orderId')
    .get(OrderController.findById)
    .delete( requireAuth,OrderController.delete);
router.route('/:orderId/cancel')
    .put( requireAuth,OrderController.cancel)

router.route('/:orderId/receive')
    .put( requireAuth,OrderController.receive)

router.route('/:orderId/onTheWay')
    .put( requireAuth,OrderController.onTheWay)

router.route('/:orderId/arrived')
    .put( requireAuth,OrderController.arrived)  

router.route('/:orderId/delivered')
    .put( requireAuth,OrderController.delivered)  

router.route('/:orderId/rate')
    .put( requireAuth,OrderController.rate)  

router.route('/:orderId/paidFromBalance')
    .put( requireAuth,OrderController.payFromBalance)  

router.route('/upload')
    .post( requireAuth,
            multerSaveTo('orders').single('img'),
            OrderController.uploadImage
        )  
router.route('/checkCoupon')
    .post( requireAuth,OrderController.checkCoupon)  
    
export default router;
